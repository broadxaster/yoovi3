//
//  YVPhoto.m
//  Yoovi3
//
//  Created by Asaf Inbar on 4/9/14.
//  Copyright (c) 2014 Yoovi. All rights reserved.
//

#import "YVPhoto.h"

@implementation YVPhoto

-(instancetype)initWithImageName:(NSString*)imageName{
    self = [super init];
    if (self) {
        self.photoId = imageName;
        self.image = [UIImage imageNamed:imageName];
    }
    return self;
}

@end

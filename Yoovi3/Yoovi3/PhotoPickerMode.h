//
//  PhotoPickerMode.h
//  Yoovi
//
//  Created by eytan levit on 12/3/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//


typedef enum{
    PhotoPickerModeMultiple,
    PhotoPickerModeSinglePhoto
} PhotoPickerMode;
//
//  YVGalleryCell.h
//  Yoovi3
//
//  Created by Asaf Inbar on 4/13/14.
//  Copyright (c) 2014 Yoovi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YVGalleryCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhoto;

@end

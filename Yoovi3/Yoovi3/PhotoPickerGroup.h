//
//  PhotoPickerGroup.h
//  Yoovi
//
//  Created by eytan levit on 11/13/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "YooviModel.h"

@interface PhotoPickerGroup : YooviModel

-(id)initWithGroupObject:(ALAssetsGroup *)groupObject;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) UIImage *coverPhoto;
@property NSInteger numberOfPhotos;
@property ALAssetsGroupType groupType;

@property (nonatomic, strong) ALAssetsGroup *groupObject;

@end

//
//  PhotoPickerGroups.h
//  Yoovi
//
//  Created by Asaf Inbar on 10/29/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoPickerGroup.h"
#import "YooviModel.h"

@interface PhotoPickerGroups : YooviModel

@property (nonatomic, strong) NSMutableArray *groups;
@property (nonatomic) NSUInteger selectedGroup;

-(id)initWithGroups:(NSMutableArray *)groups;
//-(PhotoPickerGroup *)getSelectedGroup;
-(PhotoPickerGroup *)getCameraRollGroup;

@end
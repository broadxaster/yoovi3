//
//  NewApi+PhotoPicker.m
//  Yoovi
//
//  Created by eytan levit on 12/17/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import "NewApi+PhotoPicker.h"
#import <AssetsLibrary/AssetsLibrary.h>

@implementation NewApi (PhotoPicker)

-(void)getPhotoGroupsModelWithBlock:(ApiBlock)apiBlock{
    NSAssert(apiBlock, @"apiBlock must be set.. I mean.. that's the whole purpose of this method.. it's not optional dog!");
    apiBlock(DataStateLoading, nil, nil);
//    apiBlock(DataStateNoPermissions, nil, nil);
//    return;
    NSMutableArray *groups = [[NSMutableArray alloc] init];
    ALAssetsLibraryGroupsEnumerationResultsBlock listGroupBlock =
    ^(ALAssetsGroup *group, BOOL *stop) {
        if(group){
            PhotoPickerGroup *groupModel = [[PhotoPickerGroup alloc] initWithGroupObject:group];
            
            if ([[group valueForProperty:ALAssetsGroupPropertyType] intValue] == ALAssetsGroupSavedPhotos) {
                [groups insertObject:groupModel atIndex:0];
            }
            else {
                [groups addObject:groupModel];
            }
        }
        else{
            if(groups.count == 0){
                apiBlock(DataStateEmpty, nil,nil);
            }
            else{
                apiBlock(DataStateRealData, [[PhotoPickerGroups alloc] initWithGroups:groups], nil);
            }
        }
    };
    
    ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error) {
        switch ([error code]) {
            case ALAssetsLibraryAccessUserDeniedError:
            case ALAssetsLibraryAccessGloballyDeniedError:
                apiBlock(DataStateNoPermissions, nil, nil);
                break;
            default:
                apiBlock(DataStateError, nil, error);
                break;
        }
    };
    
    NSUInteger groupTypes = ALAssetsGroupAll | ALAssetsGroupEvent | ALAssetsGroupFaces;
    [self.assetsLibrary enumerateGroupsWithTypes:groupTypes usingBlock:listGroupBlock failureBlock:failureBlock];
}

-(void)getPhotoPickerModelForGroup:(PhotoPickerGroup *)group withBlock:(ApiBlock)apiBlock{
    NSAssert(apiBlock && group, @"Dog!! group & apiBlock must be set.. I mean.. that's the whole purpose of this method.. these parameters are NOT optional!");
    
    apiBlock(DataStateLoading, nil,nil);
    [group.groupObject setAssetsFilter:[ALAssetsFilter allPhotos]];
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    
    ALAssetsGroupEnumerationResultsBlock resultsBlock = ^(ALAsset *asset, NSUInteger index, BOOL *stop){
        if (asset){
            NSString *photoId = [[asset valueForProperty:ALAssetPropertyAssetURL] absoluteString];
            PhotoPickerPhoto *photoPickerPhoto =[PhotoPickerPhoto photoWithPhotoId:photoId Photo:nil];
            photoPickerPhoto.asset = asset;
            
            [photos addObject:photoPickerPhoto];
        }
        else{
            if(photos.count == 0){
                apiBlock(DataStateEmpty, nil, nil);
            }
            else{
                PhotoPicker *photoPickerModel = [[PhotoPicker alloc] initWithPhotos:(NSArray *)photos];
                apiBlock(DataStateRealData, photoPickerModel, nil);
            }
        
            NSLog(@"getPhotoPickerForGroup model count %d", photos.count);
        }
    };
    
    [group.groupObject enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:resultsBlock];
}

@end

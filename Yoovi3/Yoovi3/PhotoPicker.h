//
//  PhotoPicker.h
//  Yoovi
//
//  Created by eytan levit on 10/20/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoPickerPhoto.h"
#import "PhotoPickerMode.h"
#import "YooviModel.h"

@interface PhotoPicker : YooviModel

-(id)initWithPhotos:(NSArray *)photos;

@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic) PhotoPickerMode mode;

@end
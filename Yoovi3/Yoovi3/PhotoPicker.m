//
//  PhotoPicker.m
//  Yoovi
//
//  Created by eytan levit on 10/20/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import "PhotoPicker.h"

@implementation PhotoPicker

-(id)initWithPhotos:(NSMutableArray *)photos{
    self = [super init];
    
    if(self){
        _photos = photos;
    }
    
    return self;
}

@end
//
//  YooviView.h
//  AddMember
//
//  Created by eytan levit on 11/20/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataState.h"
#import "YooviModel.h"

@interface YooviView : UIView

-(void)setup;
-(void)loadData:(DataState)dataState Model:(YooviModel *)model Error:(NSError *)error Delegate:(id)delegate;

@property (nonatomic, strong) id delegate;
@property (nonatomic, strong) IBOutlet UIView *error;
@property (nonatomic, strong) IBOutlet UIView *loading;
@property (nonatomic, strong) IBOutlet UIView *empty;
@property (nonatomic, strong) IBOutlet UIView *noPermissions;
@property (nonatomic, strong) IBOutlet UIView *data;

@end

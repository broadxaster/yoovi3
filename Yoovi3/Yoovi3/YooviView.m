//
//  YooviView.m
//  AddMember
//
//  Created by eytan levit on 11/20/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import "YooviView.h"

@implementation YooviView

-(void)loadData:(DataState)dataState Model:(YooviModel *)model Error:(NSError *)error Delegate:(id)delegate{
    _delegate = delegate;
    [_error setHidden:(dataState != DataStateError)];
    [_loading setHidden:(dataState != DataStateLoading)];
    [_empty setHidden:(dataState != DataStateEmpty)];
    [_noPermissions setHidden:(dataState != DataStateNoPermissions)];
    [_data setHidden:((dataState != DataStateCaching) && (dataState != DataStateRealData))];
}

-(void)setup{
    // do nothin' only for subclassing purposes
}

@end

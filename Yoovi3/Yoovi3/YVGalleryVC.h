//
//  YVGalleryVC.h
//  Yoovi3
//
//  Created by Asaf Inbar on 4/9/14.
//  Copyright (c) 2014 Yoovi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YVGallery.h"
#import "YVPhoto.h"
#import "YVGalleryCell.h"

@interface YVGalleryVC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) YVGallery *model;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionGallery;
-(void)loadModel:(YVGallery*)galleryModel;
@end

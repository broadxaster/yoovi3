//
//  Api.h
//  AddMember
//
//  Created by eytan levit on 11/20/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataState.h"
#import "YooviModel.h"
#import <AssetsLibrary/AssetsLibrary.h>

typedef void (^ApiBlock)(DataState state, YooviModel *model, NSError *error);
typedef void (^ApiParameterBlock)(DataState state, id parameter, NSError *error);

@interface NewApi : NSObject

+(NewApi *)sharedInstance;
@property (strong, nonatomic) ALAssetsLibrary *assetsLibrary;

@end
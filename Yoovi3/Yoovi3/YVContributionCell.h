//
//  YVContributionCell.h
//  Yoovi3
//
//  Created by Asaf Inbar on 4/14/14.
//  Copyright (c) 2014 Yoovi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YVContributionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhoto;

@end

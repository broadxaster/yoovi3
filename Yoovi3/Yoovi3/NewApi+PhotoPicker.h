//
//  NewApi+PhotoPicker.h
//  Yoovi
//
//  Created by eytan levit on 12/17/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import "NewApi.h"
#import "PhotoPickerGroups.h"
#import "PhotoPicker.h"

@interface NewApi (PhotoPicker)

-(void)getPhotoGroupsModelWithBlock:(ApiBlock)apiBlock;
-(void)getPhotoPickerModelForGroup:(PhotoPickerGroup *)group withBlock:(ApiBlock)apiBlock;

@end

//
//  YVPhoto.h
//  Yoovi3
//
//  Created by Asaf Inbar on 4/9/14.
//  Copyright (c) 2014 Yoovi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YVPhoto : NSObject
@property (retain) NSString* photoId;
@property (retain) UIImage* image;


-(instancetype)initWithImageName:(NSString*)imageName;
@end

//
//  main.m
//  Yoovi3
//
//  Created by Asaf Inbar on 4/9/14.
//  Copyright (c) 2014 Yoovi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YVAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YVAppDelegate class]));
    }
}

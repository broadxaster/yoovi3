//
//  PhotoPickerPhoto.h
//  Yoovi
//
//  Created by eytan levit on 10/20/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface PhotoPickerPhoto : NSObject

+(id)photoWithPhotoId:(NSString *)photoId Photo:(UIImage *)photo;

@property BOOL isSelected;
@property (nonatomic, strong) NSString *photoId;
@property (nonatomic, strong) UIImage *photo;
@property (nonatomic, strong) ALAsset *asset;

@end

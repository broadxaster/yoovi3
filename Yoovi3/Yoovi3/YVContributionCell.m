//
//  YVContributionCell.m
//  Yoovi3
//
//  Created by Asaf Inbar on 4/14/14.
//  Copyright (c) 2014 Yoovi. All rights reserved.
//

#import "YVContributionCell.h"

@implementation YVContributionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

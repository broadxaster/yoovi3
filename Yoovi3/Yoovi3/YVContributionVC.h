//
//  YVContributionVC.h
//  Yoovi3
//
//  Created by Asaf Inbar on 4/9/14.
//  Copyright (c) 2014 Yoovi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewApi+PhotoPicker.h"

@interface YVContributionVC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (retain) PhotoPicker* model;


@end

//
//  PhotoPickerGroup.m
//  Yoovi
//
//  Created by eytan levit on 11/13/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import "PhotoPickerGroup.h"

@implementation PhotoPickerGroup

-(id)initWithGroupObject:(ALAssetsGroup *)groupObject{
    self = [super init];
    
    if(self){
        _name = [groupObject valueForProperty:ALAssetsGroupPropertyName];
        _coverPhoto = [UIImage imageWithCGImage:[groupObject posterImage]];
        _numberOfPhotos = [groupObject numberOfAssets];
        _groupType = [[groupObject valueForProperty:ALAssetsGroupPropertyType] intValue];
        _groupObject = groupObject;
    }
    
    return self;
}

@end

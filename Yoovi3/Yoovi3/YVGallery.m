//
//  YVGallery.m
//  Yoovi3
//
//  Created by Asaf Inbar on 4/9/14.
//  Copyright (c) 2014 Yoovi. All rights reserved.
//

#import "YVGallery.h"
#import "YVPhoto.h"

@implementation YVGallery
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.photos = [[NSMutableArray alloc] init];
        [self initDemoData];
    }
    return self;
}

-(void)initDemoData{
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0713.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0612.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0010.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0011.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0015.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0017.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0019.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0020.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0069.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0083.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0084.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0085.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0086.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0095.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0096.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0098.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0104.jpg"]];
    [self.photos addObject:[[YVPhoto alloc] initWithImageName:@"IMG_0108.jpg"]];
}

@end

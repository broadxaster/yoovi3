//
//  YVGalleryVC.m
//  Yoovi3
//
//  Created by Asaf Inbar on 4/9/14.
//  Copyright (c) 2014 Yoovi. All rights reserved.
//

#import "YVGalleryVC.h"
#import "UIImage+Resize.h"
#import "UIImage+RoundedCorner.h"

@interface YVGalleryVC ()

@end

@implementation YVGalleryVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadModel:[[YVGallery alloc] init]];

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)loadModel:(YVGallery*)galleryModel{
    self.model = galleryModel;
    
}

#pragma mark collection view data surce
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.model.photos count];
    
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YVGalleryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    YVPhoto *photo = [self.model.photos objectAtIndex:indexPath.row];
    [cell.imageViewPhoto setImage:[[photo.image resizedImage:[self getSizeForIndexPath:indexPath] interpolationQuality:kCGInterpolationMedium] roundedCornerImage:10 borderSize:5]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [self getSizeForIndexPath:indexPath];
}

- (CGSize)getSizeForIndexPath:(NSIndexPath*)indexPath{
    YVPhoto *photo = [self.model.photos objectAtIndex:indexPath.row];
    CGFloat scaleFactor = self.collectionGallery.frame.size.height/photo.image.size.height;
    return CGSizeMake(photo.image.size.width * scaleFactor, photo.image.size.height * scaleFactor);
    
}


@end

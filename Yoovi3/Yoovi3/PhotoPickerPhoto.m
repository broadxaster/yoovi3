//
//  PhotoPickerPhoto.m
//  Yoovi
//
//  Created by eytan levit on 10/20/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import "PhotoPickerPhoto.h"

@implementation PhotoPickerPhoto

+(id)photoWithPhotoId:(NSString *)photoId Photo:(UIImage *)photo{
    PhotoPickerPhoto *ret = [[PhotoPickerPhoto alloc] init];
    ret.photoId = photoId;
    ret.photo = photo;
    return ret;
}

@end

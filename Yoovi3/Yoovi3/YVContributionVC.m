//
//  YVContributionVC.m
//  Yoovi3
//
//  Created by Asaf Inbar on 4/9/14.
//  Copyright (c) 2014 Yoovi. All rights reserved.
//

#import "YVContributionVC.h"
#import "YVContributionCell.h"
#import "UIImage+Resize.h"
#import "UIImage+RoundedCorner.h"

@interface YVContributionVC ()

@end

@implementation YVContributionVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadCameraRoll];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadCameraRoll{
    [[NewApi sharedInstance] getPhotoGroupsModelWithBlock:^(DataState state, YooviModel *groupsModel, NSError *error) {
        if(state == DataStateRealData){
//            _groupsModel = (PhotoPickerGroups *)groupsModel;
            PhotoPickerGroup *cameraRoll = [(PhotoPickerGroups *)groupsModel getCameraRollGroup];
            [[NewApi sharedInstance] getPhotoPickerModelForGroup:cameraRoll withBlock:^(DataState state, YooviModel *model, NSError *error) {
                if(state == DataStateRealData){
                    [self loadModel:(PhotoPicker *)model];
                }
//                [self setViewVisibility:state]; // realdata, loading or error.
//                _model = (PhotoPicker *)model;
//                _model.mode = _pickerMode;
//                [_photoPickerView loadModel:_model GroupModel:_groupsModel Delegate:self];
            }];
        }
        else{
//            [self setViewVisibility:state]; // loading, error or no permission
        }
    }];

}

-(void)loadModel:(PhotoPicker *)contributionModel{
    self.model = contributionModel;
    [self.collectionView reloadData];
    NSLog(@"contributionModel");
    
}

#pragma mark collection view data surce
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.model.photos count];
    
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YVContributionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    PhotoPickerPhoto *photo = [self.model.photos objectAtIndex:indexPath.row];
    [cell.imageViewPhoto setImage:[[[[UIImage alloc] initWithCGImage:photo.asset.thumbnail] resizedImage:[self getSizeForIndexPath:indexPath] interpolationQuality:kCGInterpolationMedium] roundedCornerImage:10 borderSize:5]];
    
    dispatch_queue_t dispatchQueue = dispatch_queue_create("com.yoovi.app", DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(dispatchQueue, ^{
        CGImageRef imageRef = photo.asset.defaultRepresentation.fullScreenImage;
        UIImage *fullRes = [UIImage imageWithCGImage:imageRef scale:0.1 orientation:UIImageOrientationUp];
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.imageViewPhoto setImage:[[fullRes resizedImage:[self getSizeForIndexPath:indexPath] interpolationQuality:kCGInterpolationMedium] roundedCornerImage:10 borderSize:5]];

        });
    });

    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [self getSizeForIndexPath:indexPath];
}

- (CGSize)getSizeForIndexPath:(NSIndexPath*)indexPath{
//    PhotoPickerPhoto *photo = [self.model.photos objectAtIndex:indexPath.row];
    return self.collectionView.bounds.size;
    
//    CGFloat scaleFactor = self.collectionGallery.frame.size.height/photo.photo.size.height;
//    return CGSizeMake(photo.photo.size.width * scaleFactor, photo.photo.size.height * scaleFactor);
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

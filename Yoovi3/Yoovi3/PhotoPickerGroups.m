//
//  PhotoPickerGroups.m
//  Yoovi
//
//  Created by Asaf Inbar on 10/29/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import "PhotoPickerGroups.h"
#import <AssetsLibrary/AssetsLibrary.h>

@implementation PhotoPickerGroups

-(id)initWithGroups:(NSMutableArray *)groups{
    self = [super init];
    
    if(self){
        _groups = groups;
        NSAssert(groups.count!=0, @"wow.. no groups?! wtf");
        _selectedGroup = 0;
    }
    
    return self;
}

//-(PhotoPickerGroup *)getSelectedGroup{
//    return [self.groups objectAtIndex:[self.selectedGroup integerValue]];
//}
-(PhotoPickerGroup *)getCameraRollGroup{
    int index = 0;

    for(int i=0;i<_groups.count;i++){
        PhotoPickerGroup *group = [_groups objectAtIndex:i];
        NSString *groupName = group.name;
        if([groupName isEqualToString:@"Camera Roll"]){
            index = i;
        }
    }
    
    return [_groups objectAtIndex:index];
}

@end

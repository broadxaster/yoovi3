//
//  Api.m
//  AddMember
//
//  Created by eytan levit on 11/20/13.
//  Copyright (c) 2013 eytanlevit. All rights reserved.
//

#import "NewApi.h"


@implementation NewApi

static NewApi *api;
static NSString *sessionId = nil;

+(NewApi *)sharedInstance{
    if(!api){
        api = [NewApi new];
    }
    
    return api;
}

-(id)init{
    self = [super init];
    
    if(self){
        _assetsLibrary = [[ALAssetsLibrary alloc] init];
    }
    
    return self;
}

@end
